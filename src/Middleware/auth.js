export default function auth({ next, router }) {
    if (!localStorage.getItem('accesToken')) {
      return router.push({ name: 'login' });
    }
    return next();
  }