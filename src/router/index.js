import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Productos from '../views/Producto/Productos.vue'
import CrearProductos from '../views/Producto/CrearProductos.vue'
import CrearAdmin from '../views/Administrador/CrearAdmin.vue'
import AdministradoresId from '../views/Administrador/AdministradoresID.vue'
import EditarAdmin from '../views/Administrador/EditarAdmin.vue'
import Configuraciones from '../views/Configuraciones/Configuraciones.vue'
import Administradores from '../views/Administrador/Administradores.vue'
import layau from '../Layouts/productosLayout.vue'
import CategoriasConfig from '../views/Configuraciones/Components/Categorias/CategoriasConfig.vue'
import AtributosConfig from '../views/Configuraciones/Components/Atributos/AtributosConfig.vue'
import ProductosId from '../views/Producto/ProductosId.vue'
import auth from '../Middleware/auth.js' 
import LoginCarga from '../components/LoginCarga.vue'

Vue.use(VueRouter)  

const routes = [
 
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },
 
  {
    path: '/',
    name: 'raiz',
    redirect: {name: 'login'}

  },
  {
    path: '/login/',
    name: 'login',
    component: Login,
  
  },
  {
    path: '/logincarga/',
    name: 'LoginCarga',
    component: LoginCarga,
  
  },

  {
    path: '/productos/',
    name: 'productos',
    component: Productos,
    meta: {
      middleware: auth,
    }
  }
  ,
  {
    path: '/productos/crear',
    name: 'crearProductos',
    component: CrearProductos,
    meta: {
      middleware: auth,
    }
  }
  ,
  {
    path: '/productos/id',
    name: 'productosid',
    component: ProductosId,
    meta: {
      middleware: auth,
    }
  }
  ,
  {
    path: '/administradores/crear',
    name: 'CrearAdmin',
    component: CrearAdmin,
    meta: {
      middleware: auth,
    }
  }
  ,
  {
    path: '/administradores',
    name: 'administradores',
    component: Administradores,
    meta: {
      middleware: auth,
    }
  }
  ,
  {
    path: '/administradores/:id',
    name: 'DescripcionAdmin',
    component: AdministradoresId,
    meta: {
      middleware: auth,
    }
  }
  ,
  {
    path: '/administradores/editar/:id',
    name: 'EditarAdmin',
    component: EditarAdmin,
    meta: {
      middleware: auth,
    }
  }
  ,
  {
    path: '/Configuraciones',
    name: 'configuraciones',
    component: Configuraciones,
    redirect: {name:'Categorias'},
    children: [{
      name: 'Categorias',
      path: '/Configuraciones',
      component: CategoriasConfig
    }
    ,
    {
      name: 'Atributos',
      path: '/Configuraciones',
      component: AtributosConfig
    }],
    meta: {
      middleware: auth,
    }
  }
  ,
  {
    path: '/layau',
    name: 'layau',
    component: layau,
    meta: {
      middleware: auth,
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
// Creates a `nextMiddleware()` function which not only
// runs the default `next()` callback but also triggers
// the subsequent Middleware function.
function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];
  // If no subsequent Middleware exists,
  // the default `next()` callback is returned.
  if (!subsequentMiddleware) return context.next;
  return (...parameters) => {
      // Run the default Vue Router `next()` callback first.
      context.next(...parameters);
      // Then run the subsequent Middleware with a new
      // `nextMiddleware()` callback.
      const nextMiddleware = nextFactory(context, middleware, index + 1);
      subsequentMiddleware({ ...context, next: nextMiddleware });
  };
}
router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
      const middleware = Array.isArray(to.meta.middleware)
          ? to.meta.middleware
          : [to.meta.middleware];
      const context = {
          from,
          next,
          router,
          to,
      };
      const nextMiddleware = nextFactory(context, middleware, 1);
      return middleware[0]({ ...context, next: nextMiddleware });
  }
  return next();
});

export default router
