import axios from "../../api/axios.js";

export default {
    storeCategoria(form) {
        return axios.post('registrar-categoria', form);
    },
    indexCategoria() {
        return axios.get('listar-categorias')
    },
    putCategoria(id, form) {
        return axios.put(`actualizar-categoria/${id}`, form)
    },
    deleteCategoria(id) {
        return axios.delete(`eliminar-categoria/${id}`)
    },
};