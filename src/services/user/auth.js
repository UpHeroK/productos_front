import axios from "../../api/axios.js";

export default {
  register(form) {
    console.log("entro xd");
    return axios.post("register", form);
  },
  login(form) {
    return axios.post("login", form);
  },
  listarAdmins() {
    return axios.get("listarusers");
  },
  traerAdmin(id) {
    return axios.get(`traeruser/${id}`);
  },
  editarAdmin(id, form) {
    return axios.put(`updateuser/${id}`, form)
  },
  borrarAdmin(id) {
    return axios.delete('deleteuser/'+id)
  },


};