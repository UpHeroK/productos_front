import axios from "../../api/axios.js";

export default {
    storeAtributo(form) {
        return axios.post(`registrar-atributo`, form);
    },
    indexAtributo() {
        return axios.get('listar-atributos')
    },
    putAtributo(id, form) {
        return axios.put(`actualizar-atributo/${id}`, form)
    },
    deleteAtributo(id) {
        return axios.delete(`eliminar-atributo/${id}`)
    },
};