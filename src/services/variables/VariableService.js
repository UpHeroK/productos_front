import axios from "../../api/axios.js";

export default {
    storeVariable(form) {
        return axios.post('registrar-variable', form);
    },
    indexVariable() {
        return axios.get('listar-variables')
    },
    putVariable(id, form) {
        return axios.put(`actualizar-variable/${id}`, form)
    },
    deleteVariable(id) {
        return axios.delete(`eliminar-variable/${id}`)
    },
};